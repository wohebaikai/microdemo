package com.rex.microdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.rex.config"})
public class MicrodemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicrodemoApplication.class, args);
    }
}
