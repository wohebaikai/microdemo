package com.rex.microdemo;

import com.rex.config.Bar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MicrodemoApplicationTests {
  @Autowired
  Bar bar;

    @Test
    public void contextLoads() {
        System.out.println(bar.getMap());
        System.out.println(bar.getName());
      System.out.println(bar.getArrs().length);
      System.out.println(bar.getNameList().get(0).get("name"));
      System.out.println(bar.getNameList().get(0).get("age"));
      System.out.println(bar.getNameList().get(1).get("name"));
      System.out.println(bar.getNameList().get(1).get("age"));

    }

}
